<?php
require_once 'Repositories\UserRepository.php';
require_once 'Crypt.php';

if (isset($_POST['submit']) && isset($_POST['email']) && isset($_POST['password'])) {
	UserRepository::InsertLoginAttempt($_POST['email'], $_SERVER['REMOTE_ADDR']);
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$result = UserRepository::GetUser($_POST['email']);
		if (Crypt::CheckPassword($_POST['password'], $result['password'])) {
			session_start();
			$_SESSION['user'] = $result['user'];
			header("Location: /welcome.php");
		} else {
			die("Error Logging In");
		}
	} else {
		die("Bad inputs");
	}
} else {
	die("Bad request");
}