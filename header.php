<?php
session_start();
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>PHP Test</title>
		<meta name="description"
			  content="php test">
		<meta name="keywords" content="php test"/>
		<meta name="author" content="Milos Stojkovic"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" href="" type="image/x-icon">

		<!-- css section -->
		<link rel="stylesheet" href="Theme/bootstrap/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="Theme/css/style.css?v=0.0.1"/>


	</head>


	<body>

		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item ">
							<a class="nav-link" href="/" style="margin-right: 15px">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/login.php">Login</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/registration.php" style="margin-right: 15px">Registration</a>
						</li>
					<?php if(isset($_SESSION['user'])){?>
						<li class="nav-item">
							<a class="nav-link" href="/logout.php" style="margin-right: 15px">Logout</a>
						</li>
					<?php }?>
					</ul>
					<form action="results.php" method="get" class="form-inline my-2 my-lg-0">
						<input class="form-control mr-sm-2" type="search" name="term" placeholder="Search" aria-label="Search">
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
					</form>
				</div>
			</nav>


		</header>