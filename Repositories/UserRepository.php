<?php

class UserRepository {

	public static function GetUser($email) {

		require "DBConnection.php";

		// prepare and bind
		$stmt = $conn->prepare("select u.UserId, u.Name, u.Email, u.DateRegistered, up.Password from user_passwords up join users u on up.UserId = u.UserId WHERE u.email = ? and up.EndDate is null");
		$stmt->bind_param("s", $email);

		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows == 0) {
			die("No user with this email");
		}

		/* bind result variables */
		mysqli_stmt_bind_result($stmt, $userId, $name, $email, $dateRegistered, $password);

		$userPassword = '';
		/* fetch value */
		while (mysqli_stmt_fetch($stmt)) {
			$user['userId'] = $userId;
			$user['name'] = $name;
			$user['email'] = $email;
			$user['dateRegistered'] = $dateRegistered;
			$userPassword = $password;
		}

		$stmt->close();
		$conn->close();

		return ['user' => $user, 'password' => $userPassword];
	}

	public static function InsertUser($name, $email, $password) {
		require_once 'Crypt.php';
		require "DBConnection.php";

		// prepare and bind
		$stmt = $conn->prepare("insert into users(`Name`, Email, DateRegistered) values(?, ?, NOW())");
		$stmt->bind_param("ss", $name, $email);

		if (!$stmt->execute()) {
			die("Error inserting new user");
		}

		$userId = $stmt->insert_id;

		$stmt = $conn->prepare("insert into user_passwords(Password, UserId, StartDate, EndDate) values(?, ?, NOW(), NULL )");
		$stmt->bind_param("ss", Crypt::HashPassword($password), $userId);

		if (!$stmt->execute()) {
			die("Error inserting user password");
		}

		$stmt->close();
		$conn->close();

		return $userId;
	}

	public static function SearchUsers($term) {

		require "DBConnection.php";

		// prepare and bind
		$stmt = $conn->prepare("select UserId, `Name`, Email, DateRegistered from users WHERE Email LIKE ? or `Name` LIKE ?");
		$term = "%" . $term . "%";
		$stmt->bind_param("ss", $term, $term);

		$stmt->execute();
		$stmt->store_result();

		/* bind result variables */
		mysqli_stmt_bind_result($stmt, $userId, $name, $email, $dateRegistered);

		$users = [];
		/* fetch value */
		while (mysqli_stmt_fetch($stmt)) {
			$user['userId'] = $userId;
			$user['name'] = $name;
			$user['email'] = $email;
			$user['dateRegistered'] = $dateRegistered;
			$users[] = $user;
		}

		$stmt->close();
		$conn->close();

		return $users;
	}

	public static function InsertLoginAttempt($email, $ip) {
		require "DBConnection.php";

		// prepare and bind
		$stmt = $conn->prepare("insert into login_attempts(Email, IPAddress, DateTime) values(?, ?, NOW())");
		$stmt->bind_param("ss", $email, $ip);

		if (!$stmt->execute()) {
			die("Error inserting lgoin attempt");
		}

		$stmt->close();
		$conn->close();

		return true;
	}

}