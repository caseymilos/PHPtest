<?php
require_once 'Repositories\UserRepository.php';
if (isset($_POST['submit']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['repeat-password'])) {
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && strlen($_POST['password']) >= 8 && $_POST['password'] == $_POST['repeat-password']) {
		$userId = UserRepository::InsertUser($_POST['name'], $_POST['email'], $_POST['password']);
		header("Location: /login.php");
	} else {
		die("Bad inputs");
	}
} else {
	die("Bad request");
}
