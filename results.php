<?php
session_start();
require_once 'Repositories\UserRepository.php';
$users = UserRepository::SearchUsers($_GET['term']);

if (isset($_SESSION['user'])) {?>
	<?php if (count($users) > 0) {?>
		<table class="table">
			<thead>
				<th>User ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Date Registered</th>
			</thead>
			<tbody>
				<?php foreach ($users as $user){?>
					<tr>
						<td><?php echo $user['userId'];?></td>
						<td><?php echo $user['name'];?></td>
						<td><?php echo $user['email'];?></td>
						<td><?php echo $user['dateRegistered'];?></td>
					</tr>
			    <?php }?>
			</tbody>
		</table>
	<?php } else {?>
		<p>No results.</p>
	<?php }?>
<?php } else {
	$notLoggedIn = true;
	include 'login.php';
}
