<?php

class Crypt {

    /**
     * @param string $rawPassword
     *
     * @return string
     */
    public static function HashPassword($rawPassword) {
        $cost = 10;
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
        $salt = sprintf("$2a$%02d$", $cost) . $salt;
        $hash = crypt($rawPassword, $salt);
        return $hash;
    }


    /**
     * @param string $rawPassword
     * @param string $hashedPassword
     *
     * @return bool
     */
    public static function CheckPassword($rawPassword, $hashedPassword) {
        return (crypt($rawPassword, $hashedPassword) == $hashedPassword);
    }

}